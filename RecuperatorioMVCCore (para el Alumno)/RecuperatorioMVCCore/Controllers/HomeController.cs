﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using RecuperatorioMVCCore.DAL;
using RecuperatorioMVCCore.Models;

namespace RecuperatorioMVCCore.Controllers
{
    public class HomeController : Controller
    {        
        [HttpGet]
        public IActionResult Index()
        {
            /***************************
             * Escriba aquí el código que cumpla con los casos de uso.
             * 
             ***************************/

            using (NombresDBContext context = new NombresDBContext())
            {
                IndexVM model = new IndexVM();
                model.cantNombres = context.Nombres.ToList().Count;
                model.nombres = context.Nombres.ToList();
                
                if (model.cantNombres > 0)
                {   
                    Nombre ultimoNombre = obtenerUltimoNombre(context);
                    model.id = ultimoNombre.Id;
                    model.valor = ultimoNombre.Valor;

                }

                return View(model);
            }
            
        }

        [HttpPost]
        public IActionResult InsertarNombre(IndexVM model)
        {
           
                using (NombresDBContext context = new NombresDBContext())
                {
                    if (ModelState.IsValid)
                    {
                        Nombre nombreAInsertar = new Nombre();
                        nombreAInsertar.Valor = model.valor;
                        context.Nombres.Add(nombreAInsertar);
                        context.SaveChanges();
                    }

                model.nombres = context.Nombres.ToList();
                model.cantNombres = model.nombres.Count;
                        
                return View("Index", model);
            }

        }

        [HttpGet]
        public IActionResult cantCaracteres()
        {
            using (NombresDBContext context = new NombresDBContext())
            {
                List<Nombre> nombres = context.Nombres.ToList();
                
                foreach (var nombre in nombres)
                {
                    nombre.Valor = nombre.Valor + "(" + nombre.Valor.ToCharArray().Count()  + ")";
                    context.SaveChanges();
                }

                return RedirectToAction("Index");
            }

        }

        [HttpGet]
        public IActionResult permutarNombres()
        {
            using (NombresDBContext context = new NombresDBContext())
            {
                Nombre primerNombre = context.Nombres.ToList().First();
                string auxiliarNombre = primerNombre.Valor;
                Nombre ultimoNombre = context.Nombres.ToList().Last();

                context.Nombres.Find(primerNombre.Id).Valor = ultimoNombre.Valor;
                context.Nombres.Find(ultimoNombre.Id).Valor = auxiliarNombre;
                context.SaveChanges();
                
                return RedirectToAction("Index");
            }

  
        }


        [HttpGet]
        public IActionResult renumerarIds()
        {
            using (NombresDBContext context = new NombresDBContext())
            {
                List<Nombre> nombresFoto = context.Nombres.ToList();
                List<Nombre> nombresEliminar = context.Nombres.ToList();
                context.Nombres.RemoveRange(nombresEliminar);
                context.SaveChanges();

                foreach (var nombre in nombresFoto)
                {
                    Nombre nombreAInsertar = new Nombre();
                    nombreAInsertar.Valor = nombre.Valor;
                    context.Nombres.Add(nombreAInsertar);
                    context.SaveChanges();
                }
                
            }
            
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult pasarMayusculas()
        {
            using (NombresDBContext context = new NombresDBContext())
            {
                List<Nombre> nombresAPasarAMayusculas = context.Nombres.ToList();
                
                foreach (var nombre in nombresAPasarAMayusculas)
                {
                    nombre.Valor = nombre.Valor.ToUpper();
                    context.SaveChanges();
                }

                return RedirectToAction("Index");
            }
        }

        [HttpGet]
        public IActionResult eliminarTodos()
        {
            using (NombresDBContext context = new NombresDBContext())
            {
                List<Nombre> nombresAEliminar = context.Nombres.ToList();
                context.Nombres.RemoveRange(nombresAEliminar);
                context.SaveChanges();
                return RedirectToAction("Index");
            }
        }



        [HttpGet]
        public IActionResult eliminarNombre(int Id)
        {
            using (NombresDBContext context = new NombresDBContext())
            {
                Nombre nombreAEliminar = context.Nombres.Find(Id);
                context.Nombres.Remove(nombreAEliminar);
                context.SaveChanges();
                return RedirectToAction("Index");
            }
        }

        [HttpGet]
        public IActionResult seleccionarNombre(int Id)
        {
            ModelState.Clear();

            using (NombresDBContext context = new NombresDBContext())
            {
                Nombre nombreASeleccionar = context.Nombres.Find(Id);
                IndexVM model = new IndexVM();
                model.nombres = context.Nombres.ToList();
                model.cantNombres = model.nombres.Count;
                model.id = nombreASeleccionar.Id;
                model.valor = nombreASeleccionar.Valor;

                return View("Index",model);
            }
        }



        Nombre obtenerUltimoNombre(NombresDBContext _context)
        {
            using (_context)
            {
                Nombre ultimoNombre = new Nombre();
                ultimoNombre = _context.Nombres.ToList().Last();

                return ultimoNombre;
            }
        }
    }
}
