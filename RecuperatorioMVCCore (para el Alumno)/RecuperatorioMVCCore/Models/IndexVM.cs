﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RecuperatorioMVCCore.DAL;
using System.ComponentModel.DataAnnotations;

namespace RecuperatorioMVCCore.Models
{
    public class IndexVM
    {
        /***************************
        * Escriba aquí el código que cumpla con los casos de uso.
        ***************************/

        public int? id { get; set; }
        
        [Required (ErrorMessage ="Por favor ingrese el valor requerido")]
        public string valor { get; set; }

        public List<Nombre> nombres { get; set; }

        public int cantNombres { get; set; }

    }
}
