USE master

IF EXISTS(select * from sys.databases where name='NombresDB')
DROP DATABASE NombresDB

CREATE DATABASE NombresDB

GO

USE [NombresDB]
GO
/****** Object:  Table [dbo].[Nombres]    Script Date: 12/1/2020 11:16:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Nombres](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Valor] [varchar](50) NULL,
 CONSTRAINT [PK_Nombres] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
